# SpringDoclet

SpringDoclet is Javadoc doclet that generates documentation on Spring artifacts in a project. The detection of
Spring artifacts is based on the presence of Spring annotations on Java classes and methods.

See the [Project Page](http://scottfrederick.github.com/springdoclet) for more information.

# 変更点

フォーク元からの変更点。

* RequestMapping のサマリに URL Parameter 列を追加（なんとなくdone）
     * @RequesrParam を埋め込む
     * @Valid は？@Model は？ @ModelAttribute は？
     * @PathVariable は？
* メソッドコメントから引っこ抜いたパラメータの説明を付けてリッチに（未）
* 引数の型から値域を推測して例示を追加（未）
* デプロイ済みのwarが生成できるようにして、アプリにアクセス可能なURL一覧を生成（未）